# LLM Chatbot

To see this project up and running, check [this demo link](https://llm-chatbot-main.vercel.app/).

## Table of Contents

- [Table of Contents](#table-of-contents)
  - [Given Brief](#given-brief)
  - [Getting Started](#-getting-started)
  - [Features](#features)
    - [Scripts Overview](#-scripts-overview)
    - [Screenshots](#screenshots)
  - [Deployment](#-deployment)
  - [Learn More](#learn-more)

## Given Brief

Build a Responsive Chatbot with Cloud-Based Backend and LLM Integration

### Objective

This project assesses your ability to design and implement both the frontend and backend components of a functional chatbot application, leveraging the power of Large Language Models (LLMs).

You'll build a responsive user interface, integrate an LLM service, and deploy a RESTful backend on a cloud platform.

### Scope:

Simple customer service bot that gets queries and returns replies from any LLM model.

### Technical Requirements

Frontend:

- UI Framework: Choose a prominent JavaScript framework (React, Vue, Angular, etc.).
- Responsive Design: Ensure the chatbot interface adapts seamlessly to desktop and mobile devices.
- Chat Interface: Design a user-friendly chat window, including:
  - Clear display of user and chatbot messages.
  - Input field for user text.
  - Support for rich content (e.g., quick reply buttons, images if applicable)

Backend:

- REST API: Develop a RESTful API to process chatbot requests and interface with the LLM.
- Cloud Deployment: Deploy the API as serverless functions on a cloud provider (AWS Lambda, Google Cloud Functions, Azure Functions, etc.)
- LLM Integration:
  - Choose an LLM service (e.g., ChatGPT, Hugging Face models).
  - Implement API calls to send user queries to the LLM and receive responses.

### Expected Behavior

- User initiates conversation, the chatbot greets the user, the user asks a question, the chatbot processes it through the LLM and provides a relevant response.
- Gracefully handle cases where the LLM provides unexpected or irrelevant responses.

### Deliverables

1. GitHub Repositories:
   - Separate repositories for frontend and backend code.
   - Clear commit messages and documentation.
2. Working Demo (Frontend): A deployed link to a live version of the chatbot interface.
3. API Documentation: Instructions on how to interact with the backend API endpoints.

### Evaluation Criteria

- Functionality: Meets the core expected behaviors.
- Code Quality (Frontend & Backend): Clean, organized, well-commented code.
- Responsiveness: Seamless UI adaptation across devices.
- LLM Integration: Successful use of the LLM for response generation.
- Cloud Deployment: Successful deployment of API as functions.
- User Experience: Overall intuitiveness and usability of the chatbot.

## 🎯 Getting Started

To get started with this project, follow these steps:

1. Fork & clone repository:

```bash
git clone https://gitlab.com/erhanyasar/llm-chatbot.git
```

2. Install the dependencies:

```bash
yarn install
```

3. Run the development server:

```bash
yarn dev
```

4. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

5. This project uses a git hook to enforce [conventional commits](https://github.com/qoomon/git-conventional-commits). To install the git hook, run the following command in the root directory of the project:

```sh
brew install pre-commit
pre-commit install -t commit-msg
```

## Features

### 📃 Scripts Overview

The following scripts are available in the `package.json`:

- `dev`: Starts the development server with colorized output
- `build`: Builds the app for production
- `start`: Starts the production server
- `lint`: Lints the code using ESLint

### Screenshots

<img width="100%" height="100%" src="./.gitlab/welcome.png">

<img width="100%" height="100%" src="./.gitlab/dialog-sample.png">

<img width="100%" height="100%" src="./.gitlab/api-doc.png">

<img width="100%" height="100%" src="./.gitlab/chat-api-doc.png">

## 🚀 Deployment

Easily deploy your Next.js app with Vercel by clicking [![Vercel](https://vercel.com/button)](https://vercel.com/)

## Learn More

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
