"use client";

import { Button, Stack, TextField } from "@mui/material";
import { useSnackbar } from "notistack";
import { useState } from "react";
import { DialogSection } from "./dialogSection";

export const ChatWrapper = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [userInput, setUserInput] = useState("");
  const [inputList, setInputList] = useState<string[]>([]);
  const [answers, setAnswers] = useState<string[]>([]);

  const { enqueueSnackbar } = useSnackbar();

  const fetchData = async () => {
    fetch("api/chat", {
      method: "POST",
      body: JSON.stringify({ textInput: userInput }),
    })
      .then((response) => response.json())
      .then((result) => setAnswers((answers) => [...answers, result.data]))
      .finally(() => setIsLoading(false));
  };

  const handleSubmitInput = (param?: string) => {
    if (userInput !== "") {
      setIsLoading(true);
      fetchData();
      setInputList((inputList) => [...inputList, userInput]);
      setUserInput("");
    } else enqueueSnackbar("Please type anything to chat", { variant: "warning" });
  };

  return (
    <Stack>
      <Stack direction="row" sx={{ marginTop: "1rem !important" }} gap={5}>
        <Stack direction="column" width="100%">
          <Stack direction="row"></Stack>
          <TextField
            variant="standard"
            sx={{ height: "5rem" }}
            value={userInput}
            onChange={(e) => setUserInput(e.target.value)}
            label="I'm an LLM Chatbot. Ask me about anything!"
          />
        </Stack>
        <Stack direction="row" gap={3} justifyContent="center" alignItems="center">
          <Button variant="contained" sx={{ height: "2.5rem" }} onClick={() => handleSubmitInput()}>
            Ask
          </Button>
          <Button
            variant="outlined"
            sx={{ padding: ".5rem !important" }}
            onClick={() => handleSubmitInput("history")}
          >
            Ask with History
          </Button>
        </Stack>
      </Stack>
      <DialogSection isLoading={isLoading} userInputs={inputList} answers={answers} />
    </Stack>
  );
};
