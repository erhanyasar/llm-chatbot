import React, { useState, useEffect } from "react";
import { Stack, Typography, Skeleton } from "@mui/material";
import { DialogSectionProps } from "../types/custom";
import { v4 as uuid } from "uuid";

export const DialogSection: React.FC<DialogSectionProps> = ({ isLoading, userInputs, answers }) => {
  const [reversedInputs, setReversedInputs] = useState<string[]>([]);
  const [reversedAnswers, setReversedAnswers] = useState<string[]>([]);

  useEffect(() => setReversedInputs(userInputs.reverse()), [userInputs]);
  useEffect(() => setReversedAnswers(answers.reverse()), [answers]);

  return (
    <Stack direction="column">
      {reversedInputs.map((dialog, index) => {
        return (
          <Stack key={uuid()} gap={1} sx={{ margin: ".5rem !important" }}>
            <Stack alignItems="flex-start">
              <Typography
                variant="body1"
                component="span"
                sx={{
                  backgroundColor: "lightgray",
                  padding: ".5rem !important;",
                  borderRadius: ".5rem",
                  minWidth: isLoading ? "3rem" : "",
                }}
              >
                {isLoading ? <Skeleton /> : reversedAnswers[index]}
              </Typography>
            </Stack>
            <Stack alignItems="flex-end" bgcolor="primary">
              <Typography
                variant="body1"
                component="span"
                sx={{
                  color: "white",
                  backgroundColor: "#1976d2",
                  padding: ".5rem !important;",
                  borderRadius: ".5rem",
                }}
              >
                {dialog}
              </Typography>
            </Stack>
          </Stack>
        );
      })}
    </Stack>
  );
};
