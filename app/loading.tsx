"use client";

import { CircularProgress } from "@mui/material";

const Loading = (): JSX.Element => <CircularProgress style={{ margin: "35% 50%" }} />;

export default Loading;