import { ReactNode } from "react";

export type ProviderOrLayoutProps = { children: ReactNode };

export type ErrorProps = {
  error: {
    message?: string;
    details?: string;
  };
  reset: () => {};
};

export type DialogSectionProps = {
  isLoading: boolean;
  userInputs: string[];
  answers: string[];
};

export type ReactSwaggerProps = {
  spec: Record<string, any>;
};