"use client";

import { Suspense } from "react";
import { AppRouterCacheProvider } from "@mui/material-nextjs/v14-appRouter";
import { SpeedInsights } from "@vercel/speed-insights/next";
import { ThemeProvider, CssBaseline } from "@mui/material";
import { SnackbarProvider } from "notistack";
import Loading from "../loading";
import theme from "@/app/utils/theme";
import { ProviderOrLayoutProps } from "@/app/types/custom";

export const AppProvider: React.FC<ProviderOrLayoutProps> = ({ children }) => {
  return (
    <Suspense fallback={<Loading />}>
      <AppRouterCacheProvider options={{ enableCssLayer: true }}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <SnackbarProvider maxSnack={3}>{children}</SnackbarProvider>
          <SpeedInsights />
        </ThemeProvider>
      </AppRouterCacheProvider>
    </Suspense>
  );
};
