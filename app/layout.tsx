import type { Metadata, Viewport } from "next";
import { Inter } from "next/font/google";
import { AppProvider } from "./store/appProvider";
import { ProviderOrLayoutProps } from "@/app/types/custom";
import "./style/globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "LLM Chatbot",
  description: "Powered with LangChain and Next",
  appleWebApp: {
    title: "LLM Chatbot",
  },
};

export const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
  maximumScale: 1,
  userScalable: false,
};

export default function RootLayout({ children }: Readonly<ProviderOrLayoutProps>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AppProvider>{children}</AppProvider>
      </body>
    </html>
  );
}
