"use client";

import Error from "./error";
import { ErrorProps } from "@/app/types/custom";

const GlobalError: React.FC<ErrorProps> = ({ error, reset }) => {
  return (
    <html>
      <body>
        <Error error={error} reset={reset} />
      </body>
    </html>
  );
};

export default GlobalError;
