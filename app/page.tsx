import { Box, Container, Typography } from "@mui/material";
import { ChatWrapper } from "./components/chatWrapper";
import styles from "./style/page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <Container maxWidth="lg" sx={{ height: "30vh" }}>
        <Box
          sx={{
            my: 4,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Typography variant="h4" component="h1">
            LLM Chatbot
          </Typography>
        </Box>
        <ChatWrapper />
      </Container>
    </main>
  );
}
