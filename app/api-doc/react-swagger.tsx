"use client";

import SwaggerUI from "swagger-ui-react";
import { ReactSwaggerProps } from "../types/custom";
import "swagger-ui-react/swagger-ui.css";

function ReactSwagger({ spec }: ReactSwaggerProps) {
  return <SwaggerUI spec={spec} />;
}

export default ReactSwagger;
