import { getApiDocs } from "@/app/lib/swagger";
import ReactSwagger from "./react-swagger";

export default async function ApiDocsPage() {
  const spec = await getApiDocs();

  return (
    <>
      <style scoped>
        {`
            body {
                height: 100vh;
            }
        `}
      </style>
      <section className="container">
        <ReactSwagger spec={spec} />
      </section>
    </>
  );
}
