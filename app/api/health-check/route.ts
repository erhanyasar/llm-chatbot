/**
 * @swagger
 * /api/health-check:
 *   get:
 *     description: Returns the health check response
 *     summary: Checks if API is up and running
 *     tags: [Health Check]
 *     responses:
 *       200:
 *         description: healt check is successful
 */

export async function GET() {
  return Response.json({ data: "healt check is successful" });
}
