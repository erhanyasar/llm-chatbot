import { NextRequest, NextResponse } from "next/server";
import { ChatOpenAI } from "@langchain/openai";
import { HumanMessage } from "@langchain/core/messages";

/**
 * @swagger
 * /api/chat:
 *   post:
 *     description: Returns the response to the chat input
 *     summary: Posts a user input and returns LLM's chat model response
 *     tags: [Chat]
 *     requestBody:
 *       description: User input
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               textInput:
 *                 type: string
 *             example:
 *                textInput: "Hey, what's up?"
 *     responses:
 *       201:
 *         description: Successful response
 *         content:
 *           application/json:
 *             example:
 *               data: "Not much, just here to help with any questions or tasks you have. How can I assist you today?"
 *       400:
 *         description: Invalid request
 */

export async function POST(req: NextRequest, res: NextResponse) {
  const chatModel = new ChatOpenAI({});
  const body = await req.json();

  const { textInput } = body;

  const messages = [new HumanMessage(textInput)];
  const model = await chatModel.invoke(messages);

  return Response.json({ data: model.content });
}
