"use client";

import NotFound from "./not-found";

export const DefaultPage = (): JSX.Element => <NotFound />;
